# Concar REST API test

*Assignment commissioned by [Concar](http://getconcar.com/).*

&nbsp;

This project uses [dropwizard](https://github.com/dropwizard/dropwizard) as
framework. For me, it was the first time using dropwizard. So things might not
be done ideally.

## How to start the Concar REST API test application

1. Run `mvn clean install` to build your application
1. Start application with `java -jar target/rest-api-test-1.0-SNAPSHOT.jar server config.yml`
1. To check that your application is running enter url `http://localhost:8080`

## Health Check

To see your applications health enter url `http://localhost:8081/healthcheck`

## Viewing the parkings on a map

* Visit `http://localhost:8080` to view all parkings on a map. This page uses the
[Google Maps API](https://developers.google.com/maps/).

## Accessing the API

* The API can be reached on `http://localhost:8080/api`. That url will return a
HTTP 404 though.
* Responses are always in JSON.
* This API uses the external
[parking API of Antwerp](http://datasets.antwerpen.be/v4/gis/carpoolparking.json).
Want 
[more information about that parking API](http://opendata.antwerpen.be/datasets/carpool-parking)
?

### Endpoint 1: Get a list of parkings

* Get all parkings: `http://localhost:8080/api/parkings`
* Get the parkings of specific type: `http://localhost:8080/api/parkings?type=Carpool`.
Filtering is done by the Antwerp parking API, it offers the same functionality,
so why would we reinvent the wheel?
* You can also use the parameters `pageSize` and `page` to navigate through data.
For example: `http://localhost:8080/api/parkings?pageSize=2&page=5`

### Endpoint 2: Get the closest parking

Note that this looks for the closest parking from bird's eye view, not using a
route. An improvement would be to calculate the distance using roads.

* Get the closest parking: `http://localhost:8080/api/parkings/closest?lat=51.068576&long=4.343817`
As you can see, you have to specify **latitude** (`lat`) and **longitude**
(`long`). Not specifying both parameters (or invalid parameters) results in a HTTP 404.
* Some more examples to test with:
    * `http://localhost:8080/api/parkings/closest?lat=51.213313&long=4.320292`
    * `http://localhost:8080/api/parkings/closest?lat=51.145011&long=4.187248`