var map;

/*
 * NOTE:
 * An appointment of mine took way longer than planned.
 * Had to rush to get something to work.
 *
 * Kept things simple. I'd like a more modern look though.
 */

function getErrorboxElement() {
	return $('div#errorMsg');
}

function getNativeMapElement() {
	return $('div#map')[0];
}

function setErrorBoxShown(shown) {
	var elem = getErrorboxElement();
	if (shown) {
		elem.css('display', 'block');
	}
	else {
		elem.css('display', 'none');
	}
}

function loadParkingMarkers() {
	// GET parkings
	$.ajax('http://localhost:8080/api/parkings', {
		success: function(data) {
			try {
				// Add parkings to map
				$.each(data.data, function(i, parking){
					// Get position
					var pos = new google.maps.LatLng(
						parking.point_lat,
						parking.point_lng
					);

		            // Add marker
					var marker = new google.maps.Marker({
						title: parking.naam,
						position: pos,
						map: map
					});
		        });
			}
			catch (err) {
				setErrorBoxShown(true);
				throw err;
			}
		},
		error: function(qXHR, textStatus, errorThrown) {
			setErrorBoxShown(true);
			throw errorThrown;
		}
	});
}

function initMap() {
	/* Based on:
	 * https://developers.google.com/maps/documentation/javascript/adding-a-google-map
	 */

	// Load the Google Map
	var belgianCenterPoint = {lat: 50.503882, lng: 4.469592};
	map = new google.maps.Map(getNativeMapElement(), {
		zoom: 8,
		center: belgianCenterPoint
	});

	// Load the parkings and place markers
	loadParkingMarkers();
}

/*
// Document ready
$(document).ready(function() {
	// Nothing yet
});
*/