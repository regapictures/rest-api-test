package regapictures.concar.restapitest.health;

import java.util.Set;

import com.codahale.metrics.health.HealthCheck;

import regapictures.concar.restapitest.api.Parking;
import regapictures.concar.restapitest.client.ParkingClient;

public class ParkingClientHealthCheck extends HealthCheck {

	private ParkingClient client;

	public ParkingClientHealthCheck(ParkingClient client) {
		this.client = client;
	}

	@Override
	protected Result check() throws Exception {
		if (this.client == null) {
			return Result.unhealthy("Parking client is null!");
		}

		try {
			Set<Parking> parkings = this.client.getParkings();
			if (parkings == null) {
				return Result.unhealthy("The parking client returned null!");
			} else if (parkings.size() == 0) {
				return Result.unhealthy("There were 0 parkings returned by the parking client!");
			} else {
				return Result.healthy("Parking client returned " + parkings.size() + " parkings.", parkings.size());
			}
		} catch (Exception e) {
			return Result.unhealthy(e);
		}
	}

}
