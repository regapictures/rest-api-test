package regapictures.concar.restapitest.api;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Container for data.
 * 
 * @author regapictures
 *
 * @param <E>
 *            The type of data
 */
public class Data<E> {

	/**
	 * The data.
	 */
	protected E data;

	public Data() {
		this(null);
	}

	public Data(E data) {
		this.data = data;
	}

	@JsonProperty
	public E getData() {
		return this.data;
	}

	@JsonProperty
	public void setData(E data) {
		this.data = data;
	}
}
