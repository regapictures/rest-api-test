package regapictures.concar.restapitest.api;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Object containing a {@link Parking} and the distance to that parking.
 * 
 * @author regapictures
 *
 */
public class ParkingDistance {

	/**
	 * The parking.
	 */
	private Parking parking;

	/**
	 * The distance
	 */
	private double directDistance;

	public ParkingDistance(Parking parking, double directDistance) {
		this.parking = parking;
		this.directDistance = directDistance;
	}

	@JsonProperty
	public Parking getParking() {
		return this.parking;
	}

	@JsonProperty
	public void setParking(Parking parking) {
		this.parking = parking;
	}

	@JsonProperty
	public double getDirectDistance() {
		return this.directDistance;
	}

	@JsonProperty
	public void setDirectDistance(double directDistance) {
		this.directDistance = directDistance;
	}
}
