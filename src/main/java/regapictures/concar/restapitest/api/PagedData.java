package regapictures.concar.restapitest.api;

import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.dropwizard.jackson.JsonSnakeCase;

@JsonSnakeCase // To match the style of the parking json.
/**
 * POJO class to allow paged JSON responses to clients.
 * 
 * @author regapictures
 *
 * @param <E>
 *            The collection type this {@link PagedData} contains
 * @param <F>
 *            The type the given collection contains
 */
public class PagedData<E extends Collection<F>, F> extends Data<E> {

	/**
	 * Reference to the complete Collection of data. This field is not converted
	 * to JSON.
	 */
	private E allData;

	/*
	 * BELOW: Fields that get converted to JSON
	 */

	/**
	 * The size of the collection.
	 */
	private int records;

	/**
	 * The offset index (zero indexed).
	 */
	private int offset;

	/**
	 * The amount of records on a page.
	 */
	private int pageSize;

	/**
	 * Current page number (Starts on 1, not 0).
	 */
	private int currentPage;

	/**
	 * The amount of pages.
	 */
	private int pageCnt;

	/**
	 * Creates a new {@link PagedData}.
	 * 
	 * 
	 * @param allData
	 *            The {@link Collection} containing all data.
	 * @param emptyCollection
	 *            An empty {@link Collection} of the same type that will be used
	 *            to store all elements in of the current page. If it is not
	 *            empty, it will be cleared!
	 * @param pageSize
	 *            The amount of records per page.
	 * @param page
	 *            The current page number (starts on 1, not 0).
	 */
	public PagedData(E allData, E emptyCollection, int pageSize, int page) {
		if (allData == null) {
			throw new NullPointerException("Cannot create PagedData: parameter 'allData' should not be null!");
		} else if (emptyCollection == null) {
			throw new NullPointerException("Cannot create PagedData: parameter 'emptyCollection' should not be null!");
		} else if (pageSize < 1) {
			throw new IllegalArgumentException("Cannot create PagedData: parameter 'pageSize' should be > 0!");
		} else if (page < 1) {
			throw new IllegalArgumentException("Cannot create PagedData: parameter 'page' should be > 0!");
		}

		// emptyCollection really has to be empty!
		if (emptyCollection.size() > 0) {
			emptyCollection.clear();
		}

		// Assign fields
		this.allData = allData;
		this.data = emptyCollection;
		this.records = allData.size();
		this.pageSize = pageSize;
		this.currentPage = page;

		// Process data to created paged data
		this.createPagedData();
	}

	/**
	 * Actually creates the paged data by adding records to {@link #data}, and
	 * calculating the {@link #offset} &amp; {@link #pageCnt} values.
	 */
	private void createPagedData() {
		this.offset = this.pageSize * (this.currentPage - 1);
		this.pageCnt = Math.max((int) Math.ceil((double) this.records / this.pageSize), 1);

		/*
		 * Interface 'Collection' does not have a get(index) method!
		 */
		int i = 0;
		for (F obj : this.allData) {
			// Are we passed the data?
			if (i < (this.offset + this.pageSize)) {
				// No, are we on or passed the offset?
				if (i >= this.offset) {
					// Yes, add object.
					this.data.add(obj);
				}
			} else {
				// Stop the for loop after we passed the necessary data
				break;
			}

			i++;
		}
	}

	@JsonProperty
	public E getData() {
		return this.data;
	}

	@JsonProperty
	public void setData(E data) {
		this.data = data;
	}

	@JsonProperty
	public int getRecords() {
		return this.records;
	}

	@JsonProperty
	public void setRecords(int records) {
		this.records = records;
	}

	@JsonProperty
	public int getOffset() {
		return this.offset;
	}

	@JsonProperty
	public void setOffset(int offset) {
		this.offset = offset;
	}

	@JsonProperty
	public int getPageSize() {
		/*
		 * If there are less records on this page than the requested record
		 * count on each page, return the record count instead.
		 */
		if (this.data != null & this.data.size() < this.pageSize) {
			return this.data.size();
		} else {
			return this.pageSize;
		}
	}

	@JsonProperty
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	@JsonProperty
	public int getCurrentPage() {
		return this.currentPage;
	}

	@JsonProperty
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	@JsonProperty
	public int getPageCnt() {
		return this.pageCnt;
	}

	@JsonProperty
	public void setPageCnt(int pageCnt) {
		this.pageCnt = pageCnt;
	}
}
