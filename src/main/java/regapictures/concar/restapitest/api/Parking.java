package regapictures.concar.restapitest.api;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Parking object. Has the same fields the external Antwerp API has.
 * 
 * @author regapictures
 *
 */
public class Parking {
	private long id;
	private long objectid;
	private String point_lat;
	private String point_lng;
	private String type;
	private String gisid;
	private String naam;

	public Parking() {
		// No field assignments
		// Jackson deserialization
	}

	public Parking(long id, long objectid, String point_lat, String point_lng, String type, String gisid, String naam) {
		this.id = id;
		this.objectid = objectid;
		this.point_lat = point_lat;
		this.point_lng = point_lng;
		this.type = type;
		this.gisid = gisid;
		this.naam = naam;
	}

	@JsonProperty
	public long getId() {
		return this.id;
	}

	@JsonProperty
	public void setId(long id) {
		this.id = id;
	}

	@JsonProperty
	public long getObjectid() {
		return this.objectid;
	}

	@JsonProperty
	public void setObjectid(long objectid) {
		this.objectid = objectid;
	}

	@JsonProperty
	public String getPoint_lat() {
		return this.point_lat;
	}

	@JsonProperty
	public void setPoint_lat(String point_lat) {
		this.point_lat = point_lat;
	}

	@JsonProperty
	public String getPoint_lng() {
		return this.point_lng;
	}

	@JsonProperty
	public void setPoint_lng(String point_lng) {
		this.point_lng = point_lng;
	}

	@JsonProperty
	public String getType() {
		return this.type;
	}

	@JsonProperty
	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty
	public String getGisid() {
		return this.gisid;
	}

	@JsonProperty
	public void setGisid(String gisid) {
		this.gisid = gisid;
	}

	@JsonProperty
	public String getNaam() {
		return this.naam;
	}

	@JsonProperty
	public void setNaam(String naam) {
		this.naam = naam;
	}
}
