package regapictures.concar.restapitest;

import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import regapictures.concar.restapitest.client.ParkingClient;
import regapictures.concar.restapitest.client.ParkingClientBuilder;
import regapictures.concar.restapitest.health.ParkingClientHealthCheck;
import regapictures.concar.restapitest.resources.DefaultParkingResource;
import regapictures.concar.restapitest.resources.ParkingResource;

/**
 * Demonstration/test application commissioned by Concar.
 * 
 * I tried to use the best solution where possible, but dropwizard was new to
 * me. Therefore, some things might not be solved ideally and can be improved.
 * 
 * <p>
 * POSSIBLE IMPROVEMENTS:
 * </p>
 * <ul>
 * <li>
 * <p>
 * Better error handling: some errors could be passed to a client instead of
 * returning a HTTP 500.
 * </p>
 * <p>
 * Examples: an error due to the external parking API (e.g. 502 instead), like a
 * timeout; client errors result in HTTP 4**, etc.
 * </p>
 * </li>
 * <li>
 * <p>
 * Add JSON data representing response state. For example: 200 OK, etc. This
 * data would contain a key 'data' with the actual data, just like our paged
 * data does.
 * </p>
 * </li>
 * <li>
 * <p>
 * Use an own JSON format design for the parking data that is more consistent in
 * naming. (The Antwerp API uses a mix of CamelCase and snake_case)
 * </p>
 * </li>
 * </ul>
 * 
 * @author regapictures
 *
 */
public class ConcarRestApiTestApplication extends Application<ConcarRestApiTestConfiguration> {

	public static void main(final String[] args) throws Exception {
		new ConcarRestApiTestApplication().run(args);
	}

	@Override
	public String getName() {
		return "concar-rest-api-test";
	}

	@Override
	public void initialize(final Bootstrap<ConcarRestApiTestConfiguration> bootstrap) {
		bootstrap.addBundle(new AssetsBundle("/webroot", "/", "index.html"));
	}

	@Override
	public void run(final ConcarRestApiTestConfiguration configuration, final Environment environment) {
		environment.jersey().setUrlPattern("/api/*");
		
		// ParkingClient is built in a separate class to keep this method clean
		ParkingClient client = new ParkingClientBuilder(environment, configuration.getJerseyClientConfiguration(),
				configuration.getParkingUrl()).buildAntwerpParkingClient();

		// Create resource & register it
		ParkingResource resource = new DefaultParkingResource(client, configuration.getPaging());
		environment.jersey().register(resource);
		environment.healthChecks().register("Parking client", new ParkingClientHealthCheck(client));
	}

}
