package regapictures.concar.restapitest.resources;

import java.util.LinkedHashSet;
import java.util.Set;

import regapictures.concar.restapitest.PagingSettings;
import regapictures.concar.restapitest.api.Data;
import regapictures.concar.restapitest.api.PagedData;
import regapictures.concar.restapitest.api.Parking;
import regapictures.concar.restapitest.api.ParkingDistance;
import regapictures.concar.restapitest.client.ParkingClient;

/**
 * <p>
 * The default implementation for our Parking resource.
 * </p>
 * 
 * @author regapictures
 *
 */
public class DefaultParkingResource implements ParkingResource {

	// TODO Move to config
	private static int defaultPageSize = 50;
	private static int maxPageSize = 200;

	private final ParkingClient client;

	public DefaultParkingResource(ParkingClient client, PagingSettings pageSettings) {
		this.client = client;

		DefaultParkingResource.defaultPageSize = pageSettings.getDefaultPageSize();
		DefaultParkingResource.maxPageSize = pageSettings.getMaxPageSize();
	}

	@Override
	public PagedData<Set<Parking>, Parking> listParkings(String type, Integer pageSize, Integer page) {
		// Verify page parameters
		if (pageSize == null) {
			// Assign default value
			pageSize = DefaultParkingResource.defaultPageSize;
		} else if (pageSize > DefaultParkingResource.maxPageSize) {
			// Apply limit
			pageSize = DefaultParkingResource.maxPageSize;
		} else if (pageSize < 1) {
			// Apply limit
			pageSize = 1;
		}
		if (page == null || page < 1) {
			// Assign default value, or apply limit
			page = 1;
		}

		// Get parkings
		Set<Parking> parkings = this.client.getParkings(type);

		return new PagedData<Set<Parking>, Parking>(parkings, new LinkedHashSet<>(), pageSize, page);

		/*
		 * Alternative for filtering by type using streams:
		 * 
		 * return this.client.getParkings().parallelStream().filter(p ->
		 * p.getType().equals(type)).collect(Collectors.toCollection(
		 * LinkedHashSet::new));
		 * 
		 * This is not recommended since we have to retrieve all parkings from
		 * the remote API first.
		 */
	}

	@Override
	public Data<ParkingDistance> findClosestParking(double latitude, double longitude) {
		return new Data<>(this.client.getClosestParking(latitude, longitude));
	}
}
