package regapictures.concar.restapitest.resources;

import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import regapictures.concar.restapitest.api.Data;
import regapictures.concar.restapitest.api.PagedData;
import regapictures.concar.restapitest.api.Parking;
import regapictures.concar.restapitest.api.ParkingDistance;

@Path("/parkings")
public interface ParkingResource {

	/**
	 * <p>
	 * Returns the list of {@link Parking}s.
	 * </p>
	 * 
	 * <p>
	 * Optionally, the <code>type</code> parameter can be specified to filter by
	 * parking type.
	 * </p>
	 * 
	 * @param type
	 *            [Optional] The type of parking to filter by
	 * @param pageSize
	 *            [Optional] The size a page should be (default: 50, max: 200,
	 *            configurable)
	 * @param page
	 *            [Optional] The page number of the page to view
	 * @return The {@link Set} of {@link Parking}s, encapsulated in a paged data
	 *         container {@link PagedData}.
	 */
	@GET
	@Path("/")
	@Produces({ MediaType.APPLICATION_JSON })
	public PagedData<Set<Parking>, Parking> listParkings(@QueryParam("type") String type,
			@QueryParam("pageSize") Integer pageSize, @QueryParam("page") Integer page);

	/**
	 * <p>
	 * Searches for the closest {@link Parking} to the given location's latitude
	 * and longitude.
	 * </p>
	 * 
	 * <p>
	 * The current algorithm calculates distance from bird's eye view, not using
	 * road routes.
	 * </p>
	 * 
	 * @param latitude
	 *            (Parameter: <code>lat</code>) The latitude
	 * @param longitude
	 *            (Parameter: <code>long</code>) The longitude
	 * @return The closest parking to the given location
	 */
	@GET
	@Path("/closest")
	@Produces({ MediaType.APPLICATION_JSON })
	public Data<ParkingDistance> findClosestParking(@QueryParam("lat") double latitude,
			@QueryParam("long") double longitude);
}
