package regapictures.concar.restapitest.utils;

public class DistanceUtils {

	private DistanceUtils() {
		// Static class
	}

	/**
	 * <p>
	 * Calculates the distance between the given 2D points.
	 * </p>
	 * 
	 * <p>
	 * But the question is: is this the right way for calculating distance using
	 * latitude &amp; longitude? I don't think so. Either way, it's not a lot of
	 * work to use a different distance calculation (except if I have to
	 * implement a complex one myself).
	 * </p>
	 * 
	 * @param x1
	 *            X coordinate of point #1
	 * @param y1
	 *            Y coordinate of point #1
	 * @param x2
	 *            X coordinate of point #2
	 * @param y2
	 *            Y coordinate of point #2
	 * @return The distance between the two points
	 */
	public static double calcSimple(double x1, double y1, double x2, double y2) {
		return Math.hypot((x2 - x1), (y2 - y1));
	}

	/**
	 * <p>
	 * Calculates the distance between the two given latitude &amp; longitude
	 * points.
	 * </p>
	 * 
	 * <p>
	 * Source: <a href=
	 * "http://stackoverflow.com/questions/120283/how-can-i-measure-distance-and-create-a-bounding-box-based-on-two-latitudelongi">
	 * http://stackoverflow.com/questions/120283/how-can-i-measure-distance-and-
	 * create-a-bounding-box-based-on-two-latitudelongi</a>
	 * </p>
	 * 
	 * @param lat1
	 *            Latitude of point #1
	 * @param long1
	 *            Longitude of point #1
	 * @param lat2
	 *            Latitude of point #2
	 * @param long2
	 *            Longitude of point #2
	 * @return The distance between the two given points in meters.
	 */
	public static double calcGeo(double lat1, double long1, double lat2, double long2) {
		double earthRadius = 6371000; // meters
		double dLat = Math.toRadians(lat2 - lat1);
		double dLng = Math.toRadians(long2 - long1);
		double sindLat = Math.sin(dLat / 2);
		double sindLng = Math.sin(dLng / 2);
		double a = Math.pow(sindLat, 2)
				+ Math.pow(sindLng, 2) * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double dist = earthRadius * c;

		return dist;
	}
}
