package regapictures.concar.restapitest.client;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import regapictures.concar.restapitest.api.Parking;
import regapictures.concar.restapitest.api.ParkingDistance;
import regapictures.concar.restapitest.utils.DistanceUtils;

/**
 * <p>
 * Client implementation for the Antwerp Parking API.
 * </p>
 * 
 * <p>
 * <b>NOTE:</b> The consumed Antwerp Parking API returns their data in pages.
 * The API allows a page size up to 2000 records, while there are only 22
 * parkings at this moment. Therefore, it would not be necessary to visit
 * multiple pages as we can view this data in only one page (And we don't expect
 * the amount of parkings to grow fast).
 * </p>
 * <p>
 * But, other datasets of Antwerp's API may have many more records, that also
 * increase more rapidly. So I decided to make my algorithm page-aware when
 * reading the data, as a proof of concept for those other datasets.
 * </p>
 * 
 * 
 * @author regapictures
 *
 */
public class AntwerpParkingClient implements ParkingClient {

	private final Client client;
	private final String parkingsUrl;

	private static final String JSON_PARSE_ERR_MSG = "Error while parsing JSON of remote parking API!";

	public AntwerpParkingClient(Client client, String parkingsUrl) {
		this.client = client;
		this.parkingsUrl = parkingsUrl;
	}

	@Override
	public Set<Parking> getParkings() {
		return this.getParkings(null);
	}

	@Override
	public Set<Parking> getParkings(String parkingType) {
		/*
		 * POSSIBLE IMPROVEMENTS:
		 * 
		 * - Make this algorithm aware of the page size & number specified by
		 * our own client (that is NOT the default page size configured in the
		 * config!), and use this data in the requests this class sends to the
		 * external parking API.
		 */

		// If a parking type is given, use a query parameter for the request
		Map<String, String> queryParams = new HashMap<>();
		if (parkingType != null && !parkingType.trim().equals("")) {
			queryParams.put("type", parkingType);
		}

		// The complete list of parkings
		Set<Parking> totalSet = new LinkedHashSet<>();

		// See "POSSIBLE IMPROVEMENTS" above.
		final int PAGE_SIZE = 500;

		// Page variables
		Set<Parking> currentPageSet = null;
		int currentPage = 0;

		while (currentPageSet == null || currentPageSet.size() > 0) {
			currentPage++;
			currentPageSet = this.doPagedRequest(queryParams, PAGE_SIZE, currentPage);

			if (currentPageSet == null) {
				/*
				 * After a request, we do not expect this variable to still be
				 * null. If it is: stop the loop.
				 */
				break;
			} else {
				totalSet.addAll(currentPageSet);
			}
		}

		return totalSet;
	}

	@Override
	public ParkingDistance getClosestParking(double latitude, double longitude) {
		/*
		 * POSSIBLE IMPROVEMENTS:
		 * 
		 * - Make this algorithm aware of the page size & number specified by
		 * our own client (that is NOT the default page size configured in the
		 * config!), and use this data in the requests this class sends to the
		 * external parking API.
		 */

		// Closest parking storage
		Parking closestParking = null;
		double closestParkingDistance = Double.MAX_VALUE; // Meters

		// See "POSSIBLE IMPROVEMENTS" above.
		final int PAGE_SIZE = 500;

		// Page variables
		Set<Parking> currentPageSet = null;
		int currentPage = 0;

		while (true) {
			currentPage++;
			currentPageSet = this.doPagedRequest(new HashMap<>(), PAGE_SIZE, currentPage);

			if (currentPageSet == null || currentPageSet.size() == 0) {
				break;
			} else {
				for (Parking p : currentPageSet) {
					// Get latitude & longitude
					double parkingLat = Double.parseDouble(p.getPoint_lat());
					double parkingLong = Double.parseDouble(p.getPoint_lng());

					// Calculate distance
					double distance = DistanceUtils.calcGeo(latitude, longitude, parkingLat, parkingLong);

					/*
					 * Is the calculated distance less than the distance of the
					 * current closest parking? If so, replace it.
					 */
					if (distance < closestParkingDistance) {
						closestParking = p;
						closestParkingDistance = distance;
					}
				}
			}
		}

		return new ParkingDistance(closestParking, closestParkingDistance);
	}

	/**
	 * Does a paged request to the external API to get a part of the list of
	 * parkings, and parses the data.
	 * 
	 * @param queryParams
	 *            The query parameters to use for the request
	 * @param pageSize
	 *            The (maximum) size a page should be
	 * @param page
	 *            The current page number
	 * @return The {@link Set} of parkings on the requested page
	 */
	private Set<Parking> doPagedRequest(Map<String, String> queryParams, int pageSize, int page) {
		// Add/overwrite page parameters in queryParams
		queryParams.put("page_size", String.valueOf(pageSize));
		queryParams.put("page", String.valueOf(page));

		// Do the request to the configured URL to get the parkings
		String jsonStr = this.doRawRequest(queryParams);

		// Create an ObjectMapper
		ObjectMapper mapper = new ObjectMapper();

		// Try to parse the json & find the 'data' key
		JsonNode json = null;
		try {
			json = mapper.readTree(jsonStr).withArray("data");
		} catch (Exception e) { // Catch 'Exception' instead of 'IOException':
								// we also want to catch
								// 'UnsupportedOperationException' (thrown by
								// withArray(...))
			throw new RuntimeException(JSON_PARSE_ERR_MSG, e);
		}

		// json should not be null (withArray(...) above can return null)
		if (json == null) {
			throw new NullPointerException(JSON_PARSE_ERR_MSG);
		}

		// Try to parse the data to POJO's
		Set<Parking> parkings = null;
		try {
			/*
			 * Source (alternative available):
			 * http://stackoverflow.com/questions/6349421/how-to-use-jackson-to-
			 * deserialise-an-array-of-objects
			 */
			parkings = mapper.readValue(json.toString(), new TypeReference<LinkedHashSet<Parking>>() {
			});
		} catch (IOException e) {
			throw new RuntimeException(JSON_PARSE_ERR_MSG, e);
		}

		return parkings;
	}

	/**
	 * Does the request to the external API to get the list of parkings.
	 * 
	 * @param queryParams
	 *            The query parameters to use for the request
	 * 
	 * @return The received response. This should be a JSON String as this is
	 *         requested in the sent request.
	 */
	private String doRawRequest(Map<String, String> queryParams) {
		WebTarget target = this.client.target(this.parkingsUrl);

		// Apply queryParams, if any
		if (queryParams != null) {
			for (Entry<String, String> entry : queryParams.entrySet()) {
				// Some lists can contain null values: skip those null values!
				if (entry != null) {
					target = target.queryParam(entry.getKey(), entry.getValue());
				}
			}
		}

		return target.request(MediaType.APPLICATION_JSON).get(String.class);
	}
}
