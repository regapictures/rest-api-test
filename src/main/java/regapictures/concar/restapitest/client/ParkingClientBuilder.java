package regapictures.concar.restapitest.client;

import javax.ws.rs.client.Client;

import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.client.JerseyClientConfiguration;
import io.dropwizard.setup.Environment;

public class ParkingClientBuilder {

	private final Environment environment;
	private final JerseyClientConfiguration jerseyClientConfiguration;
	private final String parkingUrl;

	public ParkingClientBuilder(Environment environment, JerseyClientConfiguration jerseyClientConfiguration,
			String parkingUrl) {
		this.environment = environment;
		this.jerseyClientConfiguration = jerseyClientConfiguration;
		this.parkingUrl = parkingUrl;
	}

	/**
	 * Builds a {@link ParkingClient} using the {@link AntwerpParkingClient}
	 * implementation.
	 * 
	 * @return A {@link ParkingClient} using the {@link AntwerpParkingClient}
	 *         implementation
	 */
	public ParkingClient buildAntwerpParkingClient() {
		return new AntwerpParkingClient(this.buildJerseyClient(), this.parkingUrl);
	}

	/**
	 * Builds a Jersey {@link Client}. To be used by other build methods in this
	 * class
	 * 
	 * @return A Jersey {@link Client}
	 */
	private Client buildJerseyClient() {
		return new JerseyClientBuilder(this.environment).using(this.jerseyClientConfiguration)
				.build(this.environment.getName());
	}
}
