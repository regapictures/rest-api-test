package regapictures.concar.restapitest.client;

import java.util.Set;

import regapictures.concar.restapitest.api.Parking;
import regapictures.concar.restapitest.api.ParkingDistance;

/**
 * <p>
 * Client to consume the parking data of an external API.
 * </p>
 * 
 * <p>
 * Internally
 * </p>
 * 
 * @author regapictures
 *
 */
public interface ParkingClient {
	/**
	 * Returns the set of {@link Parking}s found on the external API.
	 * 
	 * @return The set of {@link Parking}s found on the external API
	 */
	public Set<Parking> getParkings();

	/**
	 * Returns the set of {@link Parking}s found on the external API, filtered
	 * by the (optionally) specified parking type.
	 * 
	 * @param parkingType
	 *            [Optional] The type of parking to filter. This can be
	 *            <code>null</code>.
	 * @return The set of {@link Parking}s found on the external API
	 */
	public Set<Parking> getParkings(String parkingType);

	/**
	 * Gets the parking closest to the given geographic location.
	 * 
	 * @param latitude
	 *            The latitude of the location
	 * @param longitude
	 *            The longitude of the location
	 * @return The closest parking
	 */
	public ParkingDistance getClosestParking(double latitude, double longitude);
}
