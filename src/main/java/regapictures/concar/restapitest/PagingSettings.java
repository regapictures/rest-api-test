package regapictures.concar.restapitest;

import javax.validation.constraints.Min;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.dropwizard.Configuration;

public class PagingSettings extends Configuration {

	@Min(1)
	/**
	 * The maximum page size
	 */
	private int maxPageSize = 200;

	@Min(1)
	/**
	 * The default page size
	 */
	private int defaultPageSize = 50;

	@JsonProperty
	public int getMaxPageSize() {
		return this.maxPageSize;
	}

	@JsonProperty
	public void setMaxPageSize(int maxPageSize) {
		this.maxPageSize = maxPageSize;
	}

	@JsonProperty
	public int getDefaultPageSize() {
		return this.defaultPageSize;
	}

	@JsonProperty
	public void setDefaultPageSize(int defaultPageSize) {
		if (defaultPageSize > this.maxPageSize) {
			defaultPageSize = this.maxPageSize;
		}
		this.defaultPageSize = defaultPageSize;
	}
}
