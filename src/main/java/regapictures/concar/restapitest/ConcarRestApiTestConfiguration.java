package regapictures.concar.restapitest;

import io.dropwizard.Configuration;
import io.dropwizard.client.JerseyClientConfiguration;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

import org.hibernate.validator.constraints.NotEmpty;

public class ConcarRestApiTestConfiguration extends Configuration {
	// TODO Test config loading: changing the config doesn't seem to do much

	@NotEmpty
	private String parkingUrl = "http://datasets.antwerpen.be/v4/gis/carpoolparking.json";

	@Valid
	@NotNull
	private JerseyClientConfiguration jerseyClient = new JerseyClientConfiguration();

	@Valid
	@NotNull
	private PagingSettings paging = new PagingSettings();

	@JsonProperty
	public String getParkingUrl() {
		return this.parkingUrl;
	}

	@JsonProperty
	public void setParkingUrl(String parkingUrl) {
		this.parkingUrl = parkingUrl;
	}

	@JsonProperty("jerseyClient")
	public JerseyClientConfiguration getJerseyClientConfiguration() {
		return this.jerseyClient;
	}

	@JsonProperty("jerseyClient")
	public void setJerseyClientConfiguration(JerseyClientConfiguration jerseyClient) {
		this.jerseyClient = jerseyClient;
	}

	@JsonProperty
	public PagingSettings getPaging() {
		return this.paging;
	}

	@JsonProperty
	public void setPaging(PagingSettings paging) {
		this.paging = paging;
	}
}
